'use strict';

// Declare app level module which depends on views, and core components
angular.module('IC2C', ['ui.router', 'ngCookies', 'lr.upload', 'ui.select', 'ngSanitize', 'ngAnimate', 'ui.bootstrap', 'ngMaterial', 'ngMessages', "ui.grid", "ui.grid.pagination", "ui.grid.resizeColumns"]).config(["$locationProvider", "$stateProvider", "$urlRouterProvider", "$sceDelegateProvider", "$httpProvider", function ($locationProvider, $stateProvider, $urlRouterProvider, $sceDelegateProvider, $httpProvider) {
  $locationProvider.baseHref = "/app/";
  $sceDelegateProvider.resourceUrlWhitelist([
    'self',
    "http://www.youtube.com/embed/**",
    "https://www.youtube.com/embed/**"
  ]);

  $stateProvider
    .state('home', {
      url: '/home',
      views: {
        content: {
          templateUrl: 'templates/home.html',
          controller: 'HomeController'
        }
      }
    })
    .state('reg1', {
      url: '/reg1/:type',
      views: {
        content: {
          templateUrl: 'templates/reg1.html',
          controller: 'Reg1Controller'
        }
      }
    })
    .state('reg2', {
      url: '/reg2/:type',
      views: {
        content: {
          templateUrl: 'templates/reg2.html',
          controller: 'Reg2Controller'
        }
      }
    })
    .state('reg3', {
      url: '/reg3/:type',
      views: {
        content: {
          templateUrl: 'templates/reg3.html',
          controller: 'Reg3Controller'
        }
      }
    })
    .state('player1', {
      url: '/player1',
      views: {
        content: {
          templateUrl: 'templates/player1.html',
          controller: 'PlayerForm1Controller'
        }
      }
    })
    .state('player2', {
      url: '/player2',
      views: {
        content: {
          templateUrl: 'templates/player2.html',
          controller: 'PlayerForm2Controller'
        }
      }
    })
    .state('player3', {
      url: '/player3',
      views: {
        content: {
          templateUrl: 'templates/player3.html',
          controller: 'PlayerForm3Controller'
        }
      }

    })
    .state('player4', {
      url: '/player4',
      views: {
        content: {
          templateUrl: 'templates/player4.html',
          controller: 'PlayerForm4Controller'
        }
      }

    })
    .state('coach1', {
      url: '/coach1',
      views: {
        content: {
          templateUrl: 'templates/coach1.html',
          controller: 'CoachForm1Controller'
        }
      }
    })
    .state('search', {
      url: '/search',
      views: {
        nav: {
          templateUrl: 'templates/navbar.html'
        },
        content: {
          templateUrl: 'templates/search.html',
          controller: 'CoachSearchController'
        }
      }
    })
    .state('results', {
      url: "/results/:queryString",
      views: {
        nav: {
          templateUrl: 'templates/navbar.html'
        },
        content: {
          templateUrl: 'templates/results.html',
        }
      }
    })
    .state('settings', {
      url: '/settings',
      views: {
        nav: {
          templateUrl: 'templates/navbar.html'
        },
        content: {
          templateUrl: 'templates/settings.html'
        }
      }
    })
    .state('profile', {
      url: '/profile/:id',
      views: {
        nav: {
          templateUrl: 'templates/navbar.html'
        },
        content: {
          templateUrl: 'templates/profile.html'
        }
      }
    })
    .state('messages', {
      url: '/messages/:id',
      views: {
        nav: {
          templateUrl: 'templates/navbar.html'
        },
        content: {
          templateUrl: 'templates/messages.html'
        }
      }
    })
    .state('feed', {
      url: '/feed',
      views: {
        nav: {
          templateUrl: 'templates/navbar.html'
        },
        content: {
          templateUrl: 'templates/feed.html'
        }
      }
    })
    .state('connect', {
      url: '/connect',
      views: {
        content: {
          templateUrl: 'templates/connect.html',
          controller: 'ConnectController'
        }
      }
    })
    .state('congrats', {
      url: '/congrats',
      views: {
        content: {
          templateUrl: 'templates/congrats.html',
          controller: 'CongratsController'
        }
      }
    })
    .state('forgotpassword', {
      url: "/forgotpassword/:id",
      views: {
        content: {
          templateUrl: "templates/forgotpassword.html",
          controller: "ForgotPasswordController"
        }
      }
    }).state('privacy', {
      url: "/privacy",
      views: {
        content: {
          templateUrl: "templates/privacy.html"
        }
      }
    }).state('contact', {
      url: "/contact",
      views: {
        content: {
          templateUrl: "templates/contact.html"
        }
      }
    }).state('mission_statement', {
      url: "/mission_statement",
      views: {
        content: {
          templateUrl: "templates/mission_statement.html"
        }
      }
    }).state('terms', {
      url: "/terms",
      views: {
        content: {
          templateUrl: "templates/terms.html"
        }
      }
    })
  $urlRouterProvider.otherwise('/home');
}]).run(["$http", "$cookies", function run($http, $cookies) {
  $http.defaults.headers.common['Authorization'] = $cookies.get('token');
}])


require('./services/index.js');
require('./controllers');
