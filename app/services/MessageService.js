'use strict';
MessageService.$inject = ['$http', 'APP_CONFIG']
function MessageService($http, APP_CONFIG){
	this.send_message = function(toUserId, message){
		return $http.post(APP_CONFIG.endpointUri+"/messages/send_message",{toUserId: toUserId, message: message}).then(function(response){
			return response.data
		})
	}
	this.messages = function(){
		return $http.get(APP_CONFIG.endpointUri+"/messages/messages").then(function(response){
			return response.data
		})
	}
}

module.exports = MessageService
