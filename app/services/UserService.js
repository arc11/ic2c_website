'use strict';
UserService.$inject = ['$http', 'APP_CONFIG']
function UserService($http, APP_CONFIG){

	this.login = function(user_email, password){
		return $http.post(APP_CONFIG.endpointUri + "/public_api/authenticate", {username: user_email, email: user_email, password: password}).then(function(res){
			return res.data
		})
	}

	this.createUser = function(name, username, email, password, promo, affilicaetsessionuuid, affilicaethitid,  type){
		return $http.post(APP_CONFIG.endpointUri + "/public_api/createUser", {username: username, name: name, email: email, password: password, promo: promo, affilicaetsessionuuid : affilicaetsessionuuid, affilicaethitid : affilicaethitid, type: type}).then(function(res){
			return res.data
		})
	}

	this.updatePhone = function(phone){
		return $http.post(APP_CONFIG.endpointUri + "/private_api/phone", {phone: phone}).then(function(res){
			return res.data
		})
	}

	this.submitVerification = function(code){
		return $http.post(APP_CONFIG.endpointUri + "/private_api/verifyPhone", {vcode: code}).then(function(res){
			return res.data
		})
	}

	this.save_edit = function(prop, val){
		return $http.post(APP_CONFIG.endpointUri + "/private_api/saveEdit", {param: prop, value: val}).then(function(res){
			return res.data
		})
	}

	this.getById = function(id){
		return $http.post(APP_CONFIG.endpointUri + "/private_api/userById", {id: id}).then(function(res){
			return res.data
		})
	}

	this.getFriends = function(token){
		return $http.post(APP_CONFIG.endpointUri + "/private_api/gapi", {token: token}).then(function(res){
			return res.data
		})
	}

	this.player1 = function(city, state, highschool, sport, coachName, coachEmail, coachPhone, bio){
		return $http.post(APP_CONFIG.endpointUri + "/private_api/player1", 
			{
				bio: bio,
				city: city,
				state: state, 
				highschool: highschool,
				sport: sport,
				coachName: coachName, 
				coachEmail: coachEmail, 
				coachPhone: coachPhone
			}).then(function(res){
			console.log(res.data)
			return res.data
		})
	}

	this.player2 = function(position, height, weight, ACT, SAT, WGPA, UWGPA, ASVAB, course, age){
		return $http.post(APP_CONFIG.endpointUri + "/private_api/player2", 
			{
				position: position,
				height: height, 
				weight: weight, 
				age: age,
				ACT: ACT, 
				SAT: SAT, 
				WGPA: WGPA, 
				UWGPA: UWGPA, 
				ASVAB: ASVAB,
				favoriteSubject: course
				
			}).then(function(res){
				console.log(res.data)
				return res.data
			})
	}

	this.coach1 = function(city, state, coachPosition, bio, school){
		return $http.post(APP_CONFIG.endpointUri + "/private_api/coach1", {
			city: city,
			state:state, 
			coachPosition: coachPosition,
			bio: bio,
			school, school
		}).then(function(response){
			return response.data
		})
	}

	this.iminterested = function(schools){
		return $http.post(APP_CONFIG.endpointUri + "/private_api/iminterested", {
			schools: schools
		}).then(function (response){
			return response.data
		})
	}

	this.interestedinme = function(schools){
		return $http.post(APP_CONFIG.endpointUri + "/private_api/interestedinme", {
			schools: schools
		}).then(function (response){
			return response.data
		})
	}

	this.uploadImage = function(bin){
		return $http.post(APP_CONFIG.endpointUri + "/private_api/upload_image", {imageData: bin}).then(function(response){
			return response.data
		})
	}

	this.sendEmail = function(address){
		return $http.post(APP_CONFIG.endpointUri + "/private_api/emailInvite", {address: address}).then(function(response){
			return response.data
		})
	}

	this.sendContactEmail = function(address, message, name){
		return $http.post(APP_CONFIG.endpointUri + "/public_api/sendContactEmail", {address: address, name: name, message: message}).then(function(response){
			return response.data
		})
	}

	this.resetPassword = function(id, password){
		return $http.post(APP_CONFIG.endpointUri + "/public_api/resetPassword", {id: id, password: password}).then(function(res){
			return res.data
		})
	}

	this.forgotPasswordEmail = function(user_email){
		return $http.post(APP_CONFIG.endpointUri + "/public_api/resetPasswordEmail", {user_email: user_email}).then(function(response){
			return response.data
		})
	}

	this.charge = function(token, email, plan){
		return $http.post(APP_CONFIG.endpointUri + "/public_api/charge", {token: token.id, email: email, plan: plan}).then(function(response){
			return response.data
		})
	}

}

module.exports = UserService