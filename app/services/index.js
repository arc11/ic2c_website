'use strict';

var GLOBALS = require('../config.globals');
var userService = require('./UserService');
var profileService = require('./ProfileService')
var searchService = require('./SearchService')
var messageService = require('./MessageService')
var feedService = require('./FeedService')
var notificationService = require('./NotificationService')
var relationService = require('./RelationService')

angular
    .module(GLOBALS.APP_NAME)
    .service('UserService', userService)
    .service('ProfileService', profileService)
    .service('SearchService', searchService)
    .service('MessageService', messageService)
    .service('FeedService', feedService)
    .service('NotificationService', notificationService)
    .service('RelationService', relationService)



