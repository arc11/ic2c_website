'use strict';
SearchService.$inject = ['$http', 'APP_CONFIG']
function SearchService($http, APP_CONFIG) {
  // this.originalSearch;
  this.initialCoaches;
  this.searchParams;

  function gettypes(checked) {
    var type1 = ''
    var type2 = ''
    console.log(checked)
    if (checked[0] && !checked[1]) {
      type1 = "player"
      type2 = 'player'
    }
    else if (!checked[0] && checked[1]) {
      type1 = "coach"
      type2 = 'coach'
    }
    else if (checked[0] && checked[1]) {
      type2 = "coach"
      type1 = "player"
    }
    console.log([type1, type2])
    return [type1, type2]
  }
  var type = undefined

  this.searchName = function (text, checked, offset) {
    type = gettypes(checked)

    return $http.post(APP_CONFIG.endpointUri + "/search/name", { searchText: text, type1: type[0], type2: type[1], offset: offset }).then(function (response) {
      for (var ret in response.data.names) {
        if (response.data.names[ret].profileImage == null || response.data.names[ret].profileImage == "null") {
          response.data.names[ret].profileImage = "images/default.png"
        }
      }
      return response.data
    })
  }
  this.searchSchool = function (text, checked, offset) {
    type = gettypes(checked)
    return $http.post(APP_CONFIG.endpointUri + "/search/school", { searchText: text, type1: type[0], type2: type[1], offset: offset }).then(function (response) {
      for (var ret in response.data.names) {
        if (response.data.names[ret].profileImage == null || response.data.names[ret].profileImage == "null") {
          response.data.names[ret].profileImage = "images/default.png"
        }
      }
      return response.data
    })
  }
  this.searchPosition = function (sport, text, checked, offset) {
    type = gettypes(checked)
    return $http.post(APP_CONFIG.endpointUri + "/search/position", { sport: sport, searchText: text, type1: type[0], type2: type[1], offset: offset }).then(function (response) {
      for (var ret in response.data.names) {
        if (response.data.names[ret].profileImage == null || response.data.names[ret].profileImage == "null") {
          response.data.names[ret].profileImage = "images/default.png"
        }
      }
      return response.data
    })
  }

  // this.searchSport = function(text, checked, offset){
  // 	type = gettypes(checked)
  // 	return $http.post(APP_CONFIG.endpointUri + "/search/sport", {searchText: text, type1: type[0], type2: type[1], offset: offset}).then(function(response){
  // 		for (var ret in response.data.names){
  // 			if(response.data.names[ret].profileImage == null || response.data.names[ret].profileImage =="null"){
  // 				response.data.names[ret].profileImage = "images/default.png"
  // 			}
  // 		}
  // 		return response.data
  // 	})
  // }
  this.getColleges = function () {
    return $http.get(APP_CONFIG.endpointUri + "/public_api/get_colleges").then(function (colleges) {
      return colleges.data
    })
  }
  this.getSports = function () {
    return $http.get(APP_CONFIG.endpointUri + "/public_api/get_sports").then(function (sports) {
      return sports.data
    })
  }
  this.sendEmail = function (name, from, to, text) {
    return $http.post(APP_CONFIG.endpointUri + "/search/send_email", { name: name, from: from, to: to, text: text }).then(function (response) {
      return response.data
    })
  }
  this.smartSearch = function (searchText) {
    return $http.post(APP_CONFIG.endpointUri + "/search/smartsearch", { searchText: searchText }).then(function (response) {
      return response.data
    })
  }

  this.coachesSearch = (searchParams, pagination) => {
    const url = "/search/coaches?" + searchParams + pagination;
    return $http.get(APP_CONFIG.endpointUri + url)
  }

  // this.setOriginalSearch = function (originalSearch) {
  //   console.log('originalSearch:', originalSearch)
  //   this.originalSearch = originalSearch;
  // }

  // this.getOriginalSearch = function () {
  //   return this.originalSearch;
  // }

  this.setInitialCoaches = function (initialCoaches) {
    this.initialCoaches = initialCoaches;
  }

  this.getInitialCoaches = function () {
    return this.initialCoaches;
  }

  this.setSearchParams = function (queryString) {
    this.searchParams = queryString;
  }

  this.getSearchParams = function () {
    return this.searchParams;
  }
}

module.exports = SearchService
