'use strict';
ProfileService.$inject = ['$http', 'APP_CONFIG']
function ProfileService($http, APP_CONFIG){
	this.getProfile = function(username){
		return $http.post(APP_CONFIG.endpointUri + "/private_api/profile", {username: username}).then(function(user){
			return user.data
		})
	}
	this.updateUser = function(user){
		return $http.post(APP_CONFIG.endpointUri + "/private_api/update_user", {user: user}).then(function(response){
			return response.data
		})
	}
}

module.exports = ProfileService
