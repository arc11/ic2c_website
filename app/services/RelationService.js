'use strict';
RelationService.$inject = ['$http', 'APP_CONFIG']
function RelationService($http, APP_CONFIG){
	this.isFollowing = function(id){
		return $http.post(APP_CONFIG.endpointUri + "/relations/isFollowing", {id: id}).then(function(response){
			return response.data
		})

	}
	this.follow = function(id){
		return $http.post(APP_CONFIG.endpointUri + "/relations/follow", {id: id}).then(function(response){
			return response.data
		})
	}
	this.unfollow = function(id){
		return $http.post(APP_CONFIG.endpointUri + "/relations/unfollow", {id: id}).then(function(response){
			return response.data
		})
	}
}
module.exports = RelationService
