'use strict';
NotificationService.$inject = ['$http', 'APP_CONFIG']
function NotificationService($http, APP_CONFIG) {
  this.getNotifications = function () {
    return $http.get(APP_CONFIG.endpointUri + "/private_api/notifications").then(function (response) {
      for (var ret in response.data) {
        if (response.data[ret].user.profileImage == null || response.data[ret].user.profileImage == "null") {
          response.data[ret].user.profileImage = "images/default.png"
        }
        response.data[ret].createdAt = response.data[ret].createdAt.split("T")[0]
      }

      return response.data
    })
  }
}

module.exports = NotificationService
