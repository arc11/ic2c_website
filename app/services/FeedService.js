'use strict';
FeedService.$inject = ['$http', 'APP_CONFIG']
function FeedService($http, APP_CONFIG){
	this.getFeed = function(){
		return $http.post(APP_CONFIG.endpointUri+"/private_api/feed", {offset: 0}).then(function(response){
			return response.data
		})
	}
}

module.exports = FeedService