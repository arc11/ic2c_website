var webpack = require('webpack');

module.exports = {
  context: __dirname,
  devtool: "inline-sourcemap",
  entry: "./app.js",
  output: {
    path: __dirname + "/",
    filename: "bundle.js"
  }
}
