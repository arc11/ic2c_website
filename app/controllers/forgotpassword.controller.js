	

ForgotPasswordController.$inject = ["$scope", "$location", "UserService", "$cookies", "$http", "$stateParams"]
function ForgotPasswordController($scope, $location, UserService, $cookies, $http, $stateParams){

	$stateParams.id
	$scope.user = {}
	$scope.user.check = [true, true, true, true, true]

	$scope.editing_password = function(){
		if($scope.password.length >= 8){
			$scope.user.check[3] = false
			$scope.password_status = "has-success"
			$scope.hide_password = true
		}
		else{
			$scope.user.check[3] = true
			$scope.password_error = "Your password must be longer than 8 characters"
			$scope.hide_password = false
			$scope.password_status = "has-warning"
		}
	}
	$scope.editing_confirm = function(){
		if($scope.confirm == $scope.password){
			$scope.user.check[4] = false
			$scope.confirm_status = "has-success"
			$scope.hide_confirm = true
		}
		else{
			$scope.user.check[4] = true
			$scope.confirm_error = "Your passwords do not match"
			$scope.hide_confirm = false
			$scope.confirm_status = "has-warning"
		}
	}

	$scope.submit = function(password){
		console.log("submitting")
		if($scope.hide_confirm && $scope.hide_password && $scope.password!= "" && $scope.username!=""){
			console.log("submitting")
			UserService.resetPassword($stateParams.id, $scope.password).then(function(response){
				if(response.success){
					console.log("Password Reset")
					$location.path('home')
				}
			})
		}
	}

}
module.exports = ForgotPasswordController