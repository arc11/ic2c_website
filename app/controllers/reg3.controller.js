Reg3Controller.$inject = ["$scope", "$location", "UserService", "$stateParams"]
function Reg3Controller($scope, $location, UserService, $stateParams){

	$scope.editing_verify = function(){
		if($scope.verify != ""){
			$scope.verify_status = "has-success"
			$scope.hide_verify = true
		}
		else{
			$scope.verify_error = "That is not a valid code."
			$scope.hide_verify = false
			$scope.verify_status = "has-warning"
		}
	}
	$scope.next_reg = function(){
		if($scope.verify_status == "has-success"){
			UserService.submitVerification($scope.verify).then(function(response){
				if(response.success){
					if($stateParams.type == "player"){
						$location.path('player1')
					}
					else{
						$location.path('coach1')
					}
				}
				else{
					$scope.hide_verify = false
					$scope.verify_status = "has-warning"
					$scope.verify_error = response.message
				}
			})
			
		}  
	}

}
module.exports = Reg3Controller;