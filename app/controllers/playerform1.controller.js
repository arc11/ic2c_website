PlayerForm1Controller.$inject = ["$scope", "$location", "UserService", 'upload']
function PlayerForm1Controller($scope, $location, UserService, upload){

	$scope.profileImage = "images/upload_pic.png"
	$scope.next_reg = function(){
		UserService.player1($scope.city, $scope.state, $scope.school, $scope.sport, $scope.CoachName, $scope.CoachEmail, $scope.CoachPhone, $scope.bio).then(function(response){
			if(response.success){
				$location.path('player2') 
			}
		})
	}
	$scope.doUpload = function () {
		var f = document.getElementById('file').files[0],
		r = new FileReader();
		r.onloadend = function(e){
			var data = e.target.result;
				UserService.uploadImage(data).then(function(response){
					$scope.profileImage = ""
					$scope.profileImage = response.profileImage

			})
		}
		r.readAsDataURL(f)
	}
}
module.exports = PlayerForm1Controller