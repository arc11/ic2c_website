SettingsController.$inject = ["$scope", "$location"]
function SettingsController($scope, $location){
	console.log("search")
	$scope.checked = [true, true, false, true, false]

	$scope.check = function(n){
		$scope.checked[n] = !$scope.checked[n]
	}
	$scope.contacts = function(){
		$location.path('/connect')
	}
}
module.exports = SettingsController