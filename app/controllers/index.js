var GLOBALS = require('../config.globals')
var homeController = require('./home.controller');
var reg1Controller = require('./reg1.controller');
var reg2Controller = require('./reg2.controller');
var reg3Controller = require('./reg3.controller');
var playerForm1 = require('./playerform1.controller');
var playerForm2 = require('./playerform2.controller');
var playerForm3 = require('./playerform3.controller');
var playerForm4 = require('./playerform4.controller');
var coachForm1 = require('./coachform1.controller');
var searchController = require('./search.controller');
var feedController = require('./feed.controller');
var settingsController = require('./settings.controller');
var messagesController = require('./messages.controller');
var profileController = require('./profile.controller');
var sidebarController = require('./sidebar.controller');
var navController = require('./nav.controller');
var connectController = require('./connect.controller');
var congratsController = require('./congrats.controller');
var forgotPasswordController = require('./forgotpassword.controller')
const coachesController = require("./coaches.controller");
const coachSearchController = require("./coachSearch.controller");

angular.module(GLOBALS.APP_NAME)
  .controller('HomeController', homeController)
  .controller('Reg1Controller', reg1Controller)
  .controller('Reg2Controller', reg2Controller)
  .controller('Reg3Controller', reg3Controller)
  .controller('PlayerForm1Controller', playerForm1)
  .controller('PlayerForm2Controller', playerForm2)
  .controller('PlayerForm3Controller', playerForm3)
  .controller('PlayerForm4Controller', playerForm4)
  .controller('CoachForm1Controller', coachForm1)
  .controller('MessagesController', messagesController)
  .controller('ProfileController', profileController)
  .controller('FeedController', feedController)
  .controller('SettingsController', settingsController)
  .controller('SearchController', searchController)
  .controller('SideBarController', sidebarController)
  .controller('NavController', navController)
  .controller('ConnectController', connectController)
  .controller('CongratsController', congratsController)
  .controller('ForgotPasswordController', forgotPasswordController)
  .controller('CoachesController', coachesController)
  .controller('CoachSearchController', coachSearchController)

