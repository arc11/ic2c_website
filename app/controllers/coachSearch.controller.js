function CoachSearchController($scope, $location, SearchService) {
  const self = this;
  $scope.initialCoachData = {};

  // Define sports
  const sportsList = require("../constants/sports");
  const divisionsList = require("../constants/divisions");
  const statesList = require("../constants/states");
  const divisionMappings = require("../constants/divisionMappings");

  $scope.sports = defineEntity(sportsList);
  $scope.divisions = defineEntity(divisionsList);
  $scope.states = defineEntity(statesList);
  $scope.search = {}
  $scope.handleSubmitSearch = () => {
    console.log("searching")
    const search = $scope.search;
    let queryString = "";
    console.log(search)
    if (!search) {
      console.log("You need to choose something")
      return
      //TODO: show an error saying you gotta choose something.
    }

    for (const param in search) {
      if (search.hasOwnProperty(param)) {
        const details = search[param];
        const { label } = details;
        let cleanedLabel = label;
        if (param === "division") {
          cleanedLabel = divisionMappings[label]
        }
        const params = `&${param}=${cleanedLabel}`;
        queryString += params;
      }
    }
    // SearchService.setOriginalSearch(search);
    handleSearch(queryString, $scope, $location, SearchService)
  }

}
module.exports = CoachSearchController;

CoachSearchController.$inject = ["$scope", "$location", "SearchService"]

function defineEntity(entitiesList) {
  const entities = [];
  for (let i = 0; i < entitiesList.length; i++) {
    const entity = entitiesList[i];
    entities.push({
      id: i,
      label: entity,
    })
  }
  return entities;
}

function handleSearch(queryString, $scope, $location, SearchService) {
  const startingPagination = `&limit=25&offset=0`
  SearchService.coachesSearch(queryString, startingPagination)
    .then(response => {
      // TODO: Handle empty set?
      const { data } = response;
      const { results } = data;
      let coachData = results;
      let pagination = {}
      if (data) {
        const { results, ...meta } = data;
        pagination = { ...meta }
      }
      if (results) {
        coachData = results.map(coach => ({
          ...coach,
          firstName: coach.name.split(" ")[0],
          lastName: coach.name.split(" ")[1]
        }))
      }

      SearchService.setSearchParams(queryString);
      SearchService.setInitialCoaches({ coachData, pagination });
      $location.path("results/" + `?${queryString}${startingPagination}`)
    }).catch(err => {
      // TODO: Handle error?
      console.log("Error in coachesSearch: ", err)
    })
}