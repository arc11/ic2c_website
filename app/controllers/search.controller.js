SearchController.$inject = ["$scope", "$location", "SearchService", "$uibModal", "$cookies"]
function SearchController($scope, $location, SearchService, $uibModal, $cookies) {




  //email_text

  $scope.username = $cookies.get('username')
  $scope.chosenSport = ""
  $scope.name = $cookies.get('name')
  $scope.email = $cookies.get('email')
  $scope.email_text = "Hi Coach, \n\nI've included a link to my iConnect2Colleges profile for you to see my highlight videos and my athletic and academic information. \nIf there is anything I can do to help you decide whether I might be a good fit, please don't hesitate to ask.\nThank you very much for your time. I hope to hear from you, and will follow up in the not to distant future.\n\nHere is my profile link: www.iconnect2colleges.com/profile/" + $scope.username + "\n\nPlease check it out if you have the time.\n\nAll the best,\n" + $scope.name
  //pagination data

  $scope.searched = false
  $scope.pagination = []
  $scope.i = 1
  $scope.active = ["active", "", "", "", ""]
  $scope.max_page = 0
  $scope.current_index = 0

  function getOffset() {
    for (i in $scope.active) {
      if ($scope.active[i] == "active") {
        return ($scope.i - 1) + i * 20
      }
    }
  }
  $scope.activate = function (index) {
    $scope.active = ["", "", "", "", ""]
    $scope.active[index] = "active"
    $scope.searchChanged()
  }
  $scope.plus = function () {
    if ($scope.i <= $scope.max_page) {
      if ($scope.active[5] == "active") {
        $scope.i += 1
      }
      else {
        $scope.active.splice(5, 1);
        $scope.active = [""].concat($scope.active)
      }
    }
    $scope.searchChanged()
  }
  $scope.minus = function () {
    if (getOffset() > 0) {
      if ($scope.active[0] == "active") {
        $scope.i -= 1
      }
      else {
        console.log("hello?")
        $scope.active.splice(0, 1);
        $scope.active = $scope.active.concat([""])
      }
    }
    $scope.searchChanged()
  }

  $scope.checked = [true, true]
  $scope.colleges = []
  $scope.pick = ""
  SearchService.getColleges().then(function (colleges) {
    $scope.colleges = colleges
  })
  SearchService.getSports().then(function (sports) {
    $scope.sports = sports
  })
  $scope.check = function (i) {
    //if schools is checked, only allow either coach or player
    if ($scope.selected2 == "bold") {
      $scope.checked = [false, false]
      $scope.checked[i] = !$scope.checked[i]
      if ($scope.checked[0]) {
        $scope.searching = "Search by high school..."
      }
      else {
        $scope.searching = "Search by college..."
      }
    }
    else {
      //I have no clue how or why this works but don't remove this line
      $scope.checked[i] = $scope.checked[i]
      console.log($scope.checked)
    }

    $scope.searchChanged()
  }
  // $scope.searchSport = function(sport){
  // 	SearchService.searchSport(sport, $scope.checked).then(function(result){
  // 		$scope.max_page = result.count/20.0
  // 		//Visibility of pagination
  // 		if(result.names.length == 20){
  // 			$scope.searched = true
  // 		}else{
  // 			$scope.searched = false
  // 		}


  // 		$scope.search_return = result.names
  // 		for (result in $scope.search_return){
  // 			if($scope.search_return[result].type == "player"){
  // 				$scope.search_return[result].player = true
  // 				$scope.search_return[result].coach = false
  // 			}
  // 			else{
  // 				$scope.search_return[result].coach = true
  // 				$scope.search_return[result].player = false
  // 			}
  // 		}
  // 	})
  // }

  $scope.searchEdited = function () {
    $scope.searched = false
    $scope.search_return = []
    $i = 0
    $scope.i = 1
    $scope.active = ["active", "", "", "", ""]
    $scope.searchChanged()
  }
  $scope.searchChanged = function () {
    if ($scope.searchText != "") {
      if ($scope.selected1 == "bold") {
        SearchService.searchName($scope.searchText, $scope.checked, getOffset()).then(function (result) {
          $scope.max_page = result.count / 20.0
          //Visibility of pagination
          if (result.names.length > 0) {
            $scope.searched = true
          } else {
            $scope.searched = false
          }


          $scope.search_return = result.names
          for (result in $scope.search_return) {

            if ($scope.search_return[result].type == "player") {
              $scope.search_return[result].player = true
              $scope.search_return[result].coach = false
            }
            else {
              $scope.search_return[result].coach = true
              $scope.search_return[result].player = false
            }
          }
        })
      }
      if ($scope.selected2 == "bold") {
        SearchService.searchSchool($scope.searchText, $scope.checked, getOffset()).then(function (result) {
          $scope.max_page = result.count / 20.0
          //Visibility of pagination
          if (result.names.length > 0) {
            $scope.searched = true
          } else {
            $scope.searched = false
          }

          $scope.search_return = result.names
          for (result in $scope.search_return) {
            if ($scope.search_return[result].type == "player") {
              $scope.search_return[result].player = true
              $scope.search_return[result].coach = false
            }
            else {
              $scope.search_return[result].coach = true
              $scope.search_return[result].player = false
            }
          }
        })
      }
    }
    if ($scope.searchText != "" || $scope.chosenSport != "") {
      if ($scope.selected3 == "bold") {

        SearchService.searchPosition($scope.chosenSport, $scope.searchText, $scope.checked, getOffset()).then(function (result) {
          $scope.max_page = result.count / 20.0
          //Visibility of pagination
          if (result.names.length > 0) {
            $scope.searched = true
          } else {
            $scope.searched = false
          }
          $scope.search_return = result.names
          for (result in $scope.search_return) {
            if ($scope.search_return[result].type == "player") {
              $scope.search_return[result].player = true
              $scope.search_return[result].coach = false
            }
            else {
              $scope.search_return[result].coach = true
              $scope.search_return[result].player = false
            }
          }
        })
      }

    }
    else {
      //Visibility of pagination
      $scope.searched = false

      $scope.search_return = []
    }
  }
  $scope.poi = ""
  $scope.uoi = ""
  $scope.searching = "Search for a person..."
  $scope.template = "myPopoverTemplate2.html"
  $scope.searchText = ""
  $scope.channge_selected_1 = function () {
    clearSearch()
    $scope.checked = [true, true]
    $scope.selected1 = "bold"
    $scope.selected2 = "unbold"
    $scope.selected3 = "unbold"
    $scope.selected4 = "unbold"
    $scope.searching = "Search for a person..."
    $scope.searchEdited()
  }
  $scope.channge_selected_2 = function () {

    clearSearch()
    $scope.checked = [true, false]
    $scope.pick = "college"
    $scope.selected1 = "unbold"
    $scope.selected2 = "bold"
    $scope.selected3 = "unbold"
    $scope.selected4 = "unbold"
    $scope.searching = "Search by high school..."
    $scope.searchEdited()
  }
  $scope.channge_selected_3 = function () {
    clearSearch()
    $scope.checked = [true, true]
    $scope.pick = "sport"
    $scope.selected1 = "unbold"
    $scope.selected2 = "unbold"
    $scope.selected3 = "bold"
    $scope.selected4 = "unbold"
    $scope.searching = "Search by position..."
    $scope.searchEdited()
  }
  $scope.channge_selected_1()

  $scope.change_poi = function (id, username, email, name, verified) {

    console.log("changing poi to " + id)
    console.log(verified)
    $scope.poi = id
    $scope.uoi = username
    $scope.eoi = email
    $scope.noi = name
    $scope.voi = verified
  }
  function clearSearch() {
    $scope.searchText = ""
    $scope.control = {}
    $scope.control.selected = null
    $scope.control.last_selected = { name: "" }
  }


  $scope.control = {}
  $scope.control.selected = null
  $scope.control.last_selected = { name: "" }

  $scope.checkSelected = function () {
    console.log("selecting")
    $scope.chosenSport = $scope.control.selected.name
    $scope.searchEdited()

  }
  $scope.change_poi_redirect = function (id, username) {
    console.log("changing poi to " + id)
    $scope.poi = id
    $scope.uoi = username
    $scope.view_profile($scope.uoi)
  }

  $scope.message = function () {
    if ($scope.poi != "") {
      $location.path('messages/' + $scope.poi)
    }
  }
  $scope.view_profile = function (username) {
    if ($scope.uoi != "") {
      $location.path('profile/' + $scope.uoi)
    }
  }
  $scope.sendEmail = function () {
    SearchService.sendEmail($scope.name, $scope.email, $scope.eoi, $scope.email_text)
  }
  $scope.emailCoach = function () {
    $scope.modalInstance = $uibModal.open({
      templateUrl: 'mailTemplate.html',
      size: 'md',
      scope: $scope
    });
  }
  $scope.contact = function () {
    console.log("contacting")
    console.log($scope.voi)
    if ($scope.voi == true) {
      $scope.message()
    }
    else {
      $scope.emailCoach()
    }
  }


}
module.exports = SearchController