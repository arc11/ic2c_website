ProfileController.$inject = ["$scope", "$location", "$uibModal", "$stateParams", "ProfileService", "SearchService", "$uibModalStack", "UserService", "$cookies", "RelationService"]
function ProfileController($scope, $location, $uibModal, $stateParams, ProfileService, SearchService, $uibModalStack, UserService, $cookies, RelationService) {

  //def stuff
  $scope.oneAtATime = true;

  $scope.profileImage = "images/upload_pic.png"

  $scope.doUpload = function () {
    var f = document.getElementById('file').files[0],
      r = new FileReader();
    r.onloadend = function (e) {
      var data = e.target.result;
      UserService.uploadImage(data).then(function (response) {
        $scope.profileImage = ""
        $scope.profileImage = response.profileImage

      })
    }
    r.readAsDataURL(f)
  }


  $scope.backgrounds = {
    "Baseball": "baseball.jpg",
    "Bowling": "bowling.jpg",
    "Cheerleading": "cheerleading.jpg",
    "Field Hockey": "fieldhockey.jpg",
    "Football": "football.jpg",
    "Men's Basketball": "basketball.jpg",
    "Men's Cross Country": "crosscountry.jpg",
    "Men's Diving": "diving.jpg",
    "Men's Fencing": "fencing.jpg",
    "Men's Golf": "golf.jpg",
    "Men's Gymnastics": "gymnastics.jpg",
    "Men's Ice Hockey": "icehockey.jpg",
    "Men's Indoor Track": "crosscountry.jpg",
    "Men's Lacrosse": "fieldhockey.jpg",
    "Men's Rowing": "rowing.jpg",
    "Men's Skiing": "skiing.jpg",
    "Men's Soccer": "soccer.jpg",
    "Men's Swimming": "diving.jpg",
    "Men's Tennis": "tennis.jpg",
    "Men's Track": "crosscountry.jpg",
    "Men's Volleyball": "",
    "Men's Water Polo": "waterpolo.jpg",
    "Softball": "softball.jpg",
    "Women's Basketball": "basketball.jpg",
    "Women's Cross Country": "crosscountry.jpg",
    "Women's Diving": "diving.jpg",
    "Women's Fencing": "fencing.jpg",
    "Women's Golf": "golf.jpg",
    "Women's Gymnastics": "gymnastics.jpg",
    "Women's Ice Hockey": "icehockey.jpg",
    "Women's Indoor Track": "crosscountry.jpg",
    "Women's Lacrosse": "fieldhockey.jpg",
    "Women's Rowing": "rowing.jpg",
    "Women's Skiing": "skiing.jpg",
    "Women's Soccer": "soccer.jpg",
    "Women's Swimming": "diving.jpg",
    "Women's Tennis": "tennis.jpg",
    "Women's Track": "crosscountry.jpg",
    "Women's Volleyball": "",
    "Women's Rifle": "rifle.jpg",
    "Women's Water Polo": "waterpolo.jpg",
    "Wrestling": "wrestling.jpg"
  }


  $scope.background = "football.jpg"


  $scope.groups = [
    {
      title: 'Dynamic Group Header - 1',
      content: 'Dynamic Group Body - 1'
    },
    {
      title: 'Dynamic Group Header - 2',
      content: 'Dynamic Group Body - 2'
    }
  ];

  $scope.items = ['Item 1', 'Item 2', 'Item 3'];


  $scope.status = {
    isCustomHeaderOpen: false,
    isFirstOpen: true,
    isFirstDisabled: false
  };
  $scope.video_embed = ""

  $scope.data_hide = true
  $scope.new = {}
  $scope.new.update_field = ""
  $scope.video_hide = true
  $scope.self = true
  $scope.coach = true
  $scope.player = false
  $scope.email_showing = false



  $scope.get_embed_link = function (url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);
    if (match && match[2].length == 11) {
      return "https://www.youtube.com/embed/" + match[2];
    } else {
      return 'error';
    }
  }


  //	fetch Initial User
  ProfileService.getProfile($stateParams.id).then(function (user) {

    $scope.show_email = function () {
      $scope.email_showing = true;
    }

    $scope.cancel_email = function () {
      $scope.email_showing = false;
    }

    $scope.send_email = function () {
      console.log($cookies.get('name'))
      SearchService.sendEmail($cookies.get('name'), $cookies.get('email'), $scope.user.email, $scope.text_area_message).then(function (res) {
        $scope.email_showing = false;
      })
    }

    $scope.view_profile = function (username) {
      $location.path('profile/' + username)
    }

    $scope.text_area_message = "Dear Coach,\n\nI am beginning to explore my college options and am very interested in attending and playing at the next level. Your school offers everything I am looking for in a college experience i.e. a university with great tradition, a renowned academic reputation, and a competitive athletic program with a winning tradition. Therefore, I want to introduce myself and give you some information about my athletic and academic backgrounds.\n\nPlease click on the link below to access all of my information including contact information, high school attending, favorite course, statistics, and academic information. You will also be able to watch my highlight reel on my Iconnect2colleges player profile.\n\nI hope you will have the opportunity to watch our team play this season. I believe I have the ability to be a part of your team, and contribute to its future success.\n\nThank you for your consideration.\n\nSincerely,\n"+$cookies.get('name')+"\nhttps://iconnect2colleges.com/#!/profile/"+$cookies.get('username')


    if (user.success) {
      $scope.user = user.user;

      if ($scope.user.profileImage == null || $scope.user.profileImage == "null") {
        $scope.user.profileImage = "images/default.png"
      }
      if ($scope.user.username == $cookies.get('username')) {
        $scope.self = false
      }
      else {
        $scope.self = true
      }
      $scope.data_hide = false
      if ($scope.user.videoLink == undefined) {
        $scope.user.videoLink = ""
      }

      if ($scope.user.type == "coach") {
        $scope.coach = true
        $scope.player = false
      }
      else {
        $scope.coach = false
        $scope.player = true
      }

      var template_sports = Object.keys($scope.backgrounds)
      $scope.background = "football.jpg"
      for (var i = 0; i < template_sports.length; i += 1) {
        if (template_sports[i].toLowerCase().includes($scope.user.sport.toLowerCase())) {
          $scope.background = $scope.backgrounds[template_sports[i]]
          break
        }
      }

    } else {
      $location.path('home')
    }
  })


  var message_key = {
    "name": "name",
    "bio": "bio",
    "sport": "sport",
    "city": "city",
    "position": "sportPosition",
    "height": "height",
    "weight": "weight",
    "age": "age",
    "bench weight": "bench",
    "SAT": "SAT",
    "ACT": "ACT",
    "ASVAB": "ASVAB",
    "WGPA": "WGPA",
    "UWGPA": "UWGPA",
    "PSAT": "PSAT",
    "video link": "videoLink",
    "coaching position": "coachPosition",
    "Favorite Subject": "favoriteSubject"
  }
  $scope.doUpload = function () {
    var f = document.getElementById('file').files[0],
      r = new FileReader();
    r.onloadend = function (e) {
      var data = e.target.result;
      UserService.uploadImage(data).then(function (response) {
        setTimeout(function () {
          $scope.$apply(function () {
            $scope.checkboxes = $scope.checkboxes.concat([{ "text": "text10", checked: true }]);
          });
        }, 1);
        $scope.user.profileImage = response.profileImage
      })
    }
    r.readAsDataURL(f)
  }


  $scope.submit_edit = function () {
    if ($scope.new.update_field != "") {
      var new_user = $scope.user
      new_user.old_videoLink = $scope.user.videoLink
      //Check to make sure that if it is a video link, it is a youtube video link
      if (message_key[$scope.editing] != "videoLink" || checkUrl($scope.new.update_field)) {
        new_user[message_key[$scope.editing]] = $scope.new.update_field
        ProfileService.updateUser(new_user).then(function (response) {
          $scope.user = response
          $uibModalStack.dismissAll();
          $scope.new.update_field = ""
        })
      }
    }
  }

  $scope.message = function () {
    $location.path('messages/' + $scope.user.id)
  }

  $scope.save_edit = function (property, value) {
    UserService.save_edit(property, value).then(function (response) {
    })
  }

  $scope.edit = function (editing) {
    if ($scope.self == false) {
      $scope.editing = editing
      $scope.modalInstance = $uibModal.open({
        templateUrl: 'myModalContent.html',
        size: 'sm',
        scope: $scope
      });
    }
  }

  $scope.navigateToSearch = function () {
    $location.path("search")
  }
  // var video_width = document.getElementById("video").clientWidth;
  // $scope.video_height = parseInt(video_width*315/parseFloat(560))

  $scope.view_profile = function (username) {
    $location.path('profile/' + username)
  }
  $scope.checkUrl = function (url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);
    if (match && match[2].length == 11) {
      return true
    }
    else {
      return false
    }
  }



}
module.exports = ProfileController