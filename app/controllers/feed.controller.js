FeedController.$inject = ["$scope", "$location", "FeedService"]
function FeedController($scope, $location, FeedService){
		$scope.show = true
		$scope.html = ""


	$scope.usernameClicked = function(index){
		console.log($scope.feed[index].user)
		$scope.view_profile($scope.feed[index].user.username)
	}
	$scope.view_profile = function(username){
		$location.path('profile/'+username)
	}
	function getId(url) {
    	var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    	var match = url.match(regExp);

    	if (match && match[2].length == 11) {
    		return 'https://www.youtube.com/embed/'+ String(match[2]);
    	}	 
    	else {
        	return 'error';
    	}
	}

	FeedService.getFeed().then(function(feed_array){
		$scope.show = false
		$scope.html = '<div class = "col-lg-5  col-xl-5 hidden-sm hidden-md"><div style = "background-color: white; height: 500px;width: 300px; position:fixed; top: 75px; right: 80px; ">ad.</div></div>'
		for (feed_element in feed_array.feed){
			if(feed_array.feed[feed_element].user === undefined){
				//check to see if this is an ad
				feed_array.feed[feed_element].user = {}
				feed_array.feed[feed_element].user.profileImage = feed_array.feed[feed_element].logo
			}
			else if(feed_array.feed[feed_element].user.profileImage === undefined || feed_array.feed[feed_element].user.profileImage == "null"){
				//check to find the image
				feed_array.feed[feed_element].user.profileImage = "images/default.png"
			}
			if(feed_array.feed[feed_element].video && feed_array.feed[feed_element].video != ""){
				//Check to see if this is a video type
				feed_array.feed[feed_element].video = getId(feed_array.feed[feed_element].video)
			}
			
		}
		$scope.feed = feed_array.feed
	})
	$scope.setWidth = function(){
  		var video_width = document.getElementById("video").clientWidth;
		$scope.video_height = parseInt(video_width*315/parseFloat(560))
	}
}
module.exports = FeedController