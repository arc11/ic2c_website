CoachForm1Controller.$inject = ["$scope", "$location", "UserService"]
function CoachForm1Controller($scope, $location, UserService){
	$scope.profileImage = "images/upload_pic.png"

  $scope.next_reg = function(){
    UserService.coach1($scope.city, $scope.state, $scope.position, $scope.bio, $scope.school).then(function(response){
      if(response.success){
        $location.path('profile') 
      }
    })
  }

	$scope.doUpload = function () {
		var f = document.getElementById('file').files[0],
      	r = new FileReader();

  r.onloadend = function(e){
    var data = e.target.result;
        UserService.uploadImage(data).then(function(response){
  			$scope.profileImage = ""
  			$scope.profileImage = response.profileImage

  	})
  }
  r.readAsDataURL(f)


  

  }
	
}
module.exports = CoachForm1Controller;