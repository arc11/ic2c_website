ConnectController.$inject = ["$scope", "$location", "$http", "UserService"]
function ConnectController($scope, $location, $http, UserService){

$scope.friend_list = []
$scope.friends = "Skip"

	var GoogleAuth; // Google Auth object.

$scope.select = "Select All"
gapi.load('client', function() {

	gapi.client.init({
			'apiKey': 'AIzaSyAnt3Zqu4W-psC9sOHd2mxOl-53qfM2TYs',
			'clientId': '123338559565-bolj7bn7orc89dnu9amgid12qpdplc71.apps.googleusercontent.com',
			'scope': 'https://www.googleapis.com/auth/drive.metadata.readonly https://www.google.com/m8/feeds/',
			'discoveryDocs': ['https://www.googleapis.com/discovery/v1/apis/people/v1/rest']
	}).then(function () {
			GoogleAuth = gapi.auth2.getAuthInstance();
	});
})


$scope.onToken = function(token) {
	console.log("Got token!")
	console.log(token);
};


$scope.google_oauth = function() {
	GoogleAuth.signIn().then(function(response) {
		gapi.client.request({
			method : 'GET', 
			path:'m8/feeds/contacts/default/full?max-results=500', 
			body : {"version":"1.0","encoding":"UTF-8"}, 
			callback : function(res) { 
				var the_array = res.feed.entry
				for (person in the_array) {
					if (the_array[person]['title']['$t'] != "") {
						var user = {}
						user.name = the_array[person]['title']['$t']
						if (the_array[person]['gd$email'] != undefined && the_array[person]['gd$email'].length > 0) {
								user.email = the_array[person]['gd$email'][0]['address']
								// user.email = "southpawac@gmail.com"
							 user.sent = "gmail"
							 $scope.friend_list.push(user)
							 $scope.friends = "Invite"
							 $scope.not_sending_email.push(true)
							 $scope.$apply()
						}
					}
				}
			}
		});
	});
}

$scope.select_all = function(){
	if($scope.select == "Select All"){
		$scope.select = "Deselect All"
		for (email in $scope.not_sending_email){
			$scope.not_sending_email[email] = false
		}
	}
	else{
		$scope.select = "Select All"
		for (email in $scope.not_sending_email){
			$scope.not_sending_email[email] = true
		}
	}
}
$scope.not_sending_email = []
$scope.addEmail = function(index){
	if($scope.not_sending_email.length>index){
		$scope.not_sending_email[index] = !$scope.not_sending_email[index]
	}
}

$scope.sendEmail = function(index){
	UserService.sendEmail($scope.friend_list[index].email).then(function(response){
		if(response.success){
$scope.friend_list[index].sent = "right"
		}
	})
}

$scope.next_reg = function(){
		for (email_index in $scope.friend_list){
			if($scope.not_sending_email[email_index] == false){
				$scope.sendEmail(email_index)
			}
		}
					$location.path('congrats')
}
	// $scope.facebook_oauth = function(){
	// 	Facebook.login(function(response) {
	// 		console.log(response)
 //        	 Facebook.api('/'+response.authResponse.userID+'/invitable_friends', function(user) {
 //        		console.log(user)
 //      		});
 //      }, {
 //      	scope: 'user_friends',
 //      	return_scopes: true
 //      });
	// }



	/**
 * Listener called when user completes auth flow. If the currentApiRequest
 * variable is set, then the user was prompted to authorize the application
 * before the request executed. In that case, proceed with that API request.
 */




}



module.exports = ConnectController;