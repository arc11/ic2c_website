


<script type="text/ng-template" id="myPopoverTemplate.html">

			 <div  class="form-group" style = "width: 200px; margin-top: 0px; padding-top: 0px;">
					<label style = "color: red;">{{login_error_text}}</label>
					<label>Email/Username</label>
					<input ng-model = "user.user_email" type="text" class="form-control">
					<label>Password</label>
					<input ng-model = "user.password" type="password" class="form-control">
					<input type="submit" class = "btn btn-success" ng-click = "signin()" style = "width: 100%; margin-top: 15px;"></input>
				</div>
				<div style = "text-align: center">
		<a ng-click = "forgotPassword()" style = "font-size: 10px;">Forgot password?</a>
		</div>
</script>

<div  ng-controller = "HomeController" id="scroll"  style = "width:100%; overflow: auto; height:100vh;overflow-x: hidden; font-family: 'Roboto Condensed', sans-serif;">
<nav id = "nav_orig" class="navbar navbar-default navbar-default-2 navbar-default-home navbar-fixed-top navbar_home" style = "background-color: #232A31">
	<div class="container-fluid">
		<div class="navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-nav-home navbar-right">
				<li style="margin-top: 12px;"><a class="nav_a hidden-xs" href="#!/home#about">About</a></li>
				<li style="margin-top: 12px;"><a class="nav_a hidden-xs" href="#!/home#testimonials">Testimonials</a></li>
				<li style="margin-top: 12px;"><a class="nav_a hidden-xs" href="#!/home#pricing">Pricing</a></li>
				<li style="margin-top: 12px; margin-left: 5px;"><a class="nav_a" href="#!/privacy">Privacy Policy</a></li>
				<li style="margin-top: 12px; margin-left: 5px; margin-right: 5px;"><a class="nav_a" href="#!/home#contact" style="font-weight: 300;">Contact</a></li>
				<li>
					<button popover-placement="bottom-right" uib-popover-template="template" popover-title="Sign in" type="button" class=" nav_btn btn btn-success btn-sm sign-in">Sign In</button>
				</li>
			</ul>
		</div>
	</div>
</nav>


<div class = "container" style = "padding-top: 100px;">
	<h1 class="contact hidden-lg hidden-md hidden-sm" style="padding:20px; ">Contact Us</h1>
	<div class="form-group" style="padding-left:20px; padding-right: 20px;">
		<input ng-model="user.contact_name" style="height: 50px;font-size: 17px;" type="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your Name *">
		<input ng-model="user.contact_email" style="height: 50px;margin-top: 30px;font-size: 17px" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your Email *">
		<textarea ng-model="user.contact_message" style="margin-top: 30px; font-size: 17px" class="form-control" placeholder="Your Message *" id="exampleTextarea" rows="6"></textarea>
		<a ng-click="sendContactEmail()" style="margin-top: 30px; float:right; background-color: white; margin-bottom: 30px" type="submit" class="btn btn-secondary btn-lg">Send</a>
	</div>
</div>

<section style = "background-color: black" name ="contact" id = "contact">
<div class = "row">
	
<p style ="font-size: 12px; color: white; width: 100%;height: 30px; text-align: center; margin-bottom: 0px;padding-top: 7px"><a style = "color: white" href = "http://linkerlogic.com/">© 2017 Linker Logic Technologies. All Rights Reserved.</a></p>


</div>
</section>