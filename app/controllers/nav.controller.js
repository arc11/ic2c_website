NavController.$inject = ["$scope", "$location", "$state", "$cookies", "NotificationService", "$timeout", "$uibModal", "SearchService"]
function NavController($scope, $location, $state, $cookies, NotificationService, $timeout, $uibModal, SearchService) {


  $scope.isopen = false;
  $scope.open = false;
  $scope.inc = 0
  $scope.searchText = ""
  $scope.users = []

  $scope.input = { searchText: "" }

  $scope.searchTextChanged = function () {
    SearchService.smartSearch($scope.input.searchText).then(function (response) {
      $scope.users = response.users
    })
  }

  $scope.toggleopen = function () {
    $scope.modalInstance = $uibModal.open({
      animation: false,
      templateUrl: 'searchbarmodal.html',
      size: 'sm',
      scope: $scope
    });


  }

  $scope.view_profile = function (username) {
    $location.path('profile/' + username)
  }
  //Todo--> Actually check if user is active from database
  $scope.not_logged_in = true;
  if ($cookies.get('name')) {
    $scope.not_logged_in = false;
  } else {
    $scope.not_logged_in = true;
  }

  $scope.badge_count = 0
  // if($cookies.get('name')){
  // 	$scope.name = $cookies.get('name')
  // }else{
  // 	$location.path('home')
  // }
  $scope.current = $state.current.name
  $scope.notif = []
  $scope.user = $cookies.get('name')

  if (!$scope.not_logged_in) {
    NotificationService.getNotifications().then(function (notifications) {
      $scope.notif = notifications
      $scope.notif.reverse()
      $scope.badge_count = 0
      for (note in notifications) {
        if (!notifications[note].seen) {
          $scope.badge_count += 1
        }
      }
    })
  }


  $scope.view_notifications = function () {
    $scope.badge_count = 0
  }
  $scope.set_active1 = function () {
    if ($cookies.get('username')) {
      var going = "profile/" + $cookies.get('username')
      $location.path(going)
    }
    else {
      $state.go('home')
    }
  }
  $scope.set_active2 = function () {
    $state.go('feed')
  }
  $scope.set_active3 = function () {
    $state.go('search')
  }
}
module.exports = NavController