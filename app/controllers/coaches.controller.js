function CoachesController($scope, $location, SearchService, uiGridConstants) {
  const self = this;
  const stateMappings = require("../constants/stateMappings");

  const { queryString: qs } = parseQueryString($location);
  const ogSearch = getParamsFromQueryString(qs);
  $scope.originalSearch = ogSearch;
  $scope.fullStateName = stateMappings[ogSearch.state] || ogSearch.state;
  $scope.resultsCount = undefined;

  self.searchCoaches = (searchParams, pagination) => {
    let queryString = searchParams;
    let paginatinString = pagination
    if (!searchParams) queryString = self.initialQueryString;

    if (!pagination) paginatinString = self.initialPaginationString;
    SearchService.coachesSearch(queryString, paginatinString)
      .then(response => {
        // TODO: Handle empty set?
        const { data } = response;
        const { results } = data;
        let coachData = results;
        if (data) {
          const { results, count, ...meta } = data;
          self.gridPagination = {
            ...meta
          }
          $scope.resultsCount = count;
        }
        if (results) {
          coachData = results.map(coach => ({
            ...coach,
            firstName: coach.name.split(" ")[0],
            lastName: coach.name.split(" ")[1]
          }))
        }
        self.gridOptions.data = coachData
      }).catch(err => {
        // TODO: Handle error?
        console.log("Error in coachesSearch: ", err)
      })
  }

  let initialSearchData = SearchService.getInitialCoaches();
  if (initialSearchData) {
    $scope.resultsCount = initialSearchData.pagination.count;
  }
  let searchParams = SearchService.getSearchParams();

  if (!initialSearchData || !searchParams) {
    const { queryString, paginationString } = parseQueryString($location);
    self.initialQueryString = queryString;
    self.initialPaginationString = paginationString;
    self.searchCoaches()
  }

  const gridColumns = [
    // {
    //   name: "Image",
    //   field: "profileImage",
    //   enableSorting: false,
    //   cellTemplate: "<img width=\"50px\" ng-src=\"{{grid.getCellValue(row, col)}}\" lazy-src>"
    // },
    {
      name: "First Name",
      field: "firstName",
      maxWidth: 150,
      // width: 450,
      // enableColumnResizing: false
    },
    {
      name: "Last Name", field: "lastName",
      maxWidth: 150,
    },
    {
      name: "School", field: "highschool", sort: {
        direction: uiGridConstants.ASC,
        priority: 0
      },
      minWidth: 250,
      // width: 450,
    },
    { name: "State", field: "state", maxWidth: 65 },
    { name: "Sport", field: "sport", maxWidth: 100 },
    { name: "Division", field: "division", maxWidth: 150 },
  ]

  self.gridOptions = {
    // maxHeight: 50,
    enableColumnResizing: true,
    enableColumnMenus: false,
    enableSorting: true,
    rowTemplate: 'templates/coaches_table/coach_row.html',
    columnDefs: gridColumns,
    enablePaginationControls: false,
    paginationPageSize: 25,
    onRegisterApi: gridApi => self.gridApi = gridApi,
  }

  if (initialSearchData) {
    self.gridOptions.data = initialSearchData.coachData
    self.gridPagination = {
      ...initialSearchData.pagination
    }
  }

  $scope.previousPage = () => self.searchCoaches(searchParams, self.gridPagination.previous)

  $scope.nextPage = () => self.searchCoaches(searchParams, self.gridPagination.next)

  $scope.handleCoachClick = row => {
    // for some reason "username" == some sort of coach uuid,
    // at any rate, it seems to be what is expected as the :id param for the profile url
    const { username } = row.entity;
    $location.path("profile/" + username);
  }
}
module.exports = CoachesController;
CoachesController.$inject = ["$scope", "$location", "SearchService", "uiGridConstants"]



/**
|--------------------------------------------------
| Local Util
|--------------------------------------------------
*/
function parseQueryString($location) {
  const path = $location.$$path
  const paramsString = path.split("?")[1];
  const splitParams = paramsString.split("&");
  let queryString = "";
  let paginationString = ""
  for (let i = 0; i < splitParams.length; i++) {
    const param = splitParams[i];
    if (param.includes("limit") || param.includes("offset")) {
      paginationString += `&${param}`
    } else if (param !== "") {
      queryString += `&${param}`
    }
  }
  return {
    queryString,
    paginationString
  }
}

function getParamsFromQueryString(qs) {
  const [_blank, ...paramsList] = qs.split("&");
  let originalSearch = {};
  for (let i = 0; i < paramsList.length; i++) {
    const [param, entry] = paramsList[i].split("=");
    originalSearch[param] = entry;
  }
  return originalSearch;
}
