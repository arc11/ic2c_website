MessagesController.$inject = ["$scope", "$location", "MessageService", "$stateParams", "UserService", "$cookies", "SearchService"]
function MessagesController($scope, $location, MessageService, $stateParams, UserService, $cookies, SearchService){

$scope.searching = false
$scope.newMessageChanged = function(){
	if($scope.search_text ==""){
		$scope.searching = false
	}
	else{
		SearchService.searchName($scope.search_text, [true, true]).then(function(result){
			$scope.search_people = result
			console.log(result)
		})
		$scope.searching = true
	}
}

$scope.message = function(id){
	$location.path('messages/'+id)
}
$scope.messages = function(){
	$scope.messages_hidden = true
	$scope.no_messages_hidden = true
	$scope.ids = []
	//getting messages
		MessageService.messages().then(function(messages){
			if(messages.length != 0){
				//If messages are received, hide the no messages screen
				$scope.messages_hidden = false
				$scope.no_messages_hidden = true

				//set the people variable equal to the returned people
				$scope.people = messages
				console.log($scope.people)
				//adding a default profile picture for every messaged user
				for(var person in $scope.people){
					$scope.ids.push($scope.people[person].index)
					if($scope.people[person].image == null ||$scope.people[person].image == "null" ){
						$scope.people[person].image = "images/default.png"
					}
				}
				// setting the default selected person
				$scope.selected_person = $scope.people[0]
			}
			else{
				//show no messages if no messages
				$scope.no_messages_hidden = false
			}
			if($stateParams.id){
				//If this is a new person
				if($scope.ids.indexOf($stateParams.id) == -1){
				$scope.messages_hidden = false
				$scope.no_messages_hidden = true
				MessageService.messages().then(function(messages){
					UserService.getById($stateParams.id).then(function(response){
						$scope.people = messages
						var new_person = {
							index: $stateParams.id,
							name: response.name,
							image: response.profileImage,
							last_message: "",
							position: response.sportPosition,
							messages: []
					}	
					if(new_person.image == "null" || new_person.image == null){
						new_person.image = "images/default.png"
					}
					$scope.people.splice(0, 0, new_person)
					$scope.people[0].seen = true
					$scope.selected_person = $scope.people[0]

					for(var person in $scope.people){
					$scope.ids.push($scope.people[person].index)
					if($scope.people[person].image == null ||$scope.people[person].image == "null" ){
						$scope.people[person].image = "images/default.png"
					}
				}
					})
				})
			}
			else{
				var idx = $scope.ids.indexOf($stateParams.id)
				var person = $scope.people[idx]
				$scope.people.splice(idx, 1)
				$scope.people.splice(0, 0, person)
				$scope.selected_person  = $scope.people[0]
			}
		}
		})
	}
	$scope.gosearch = function(){
		$location.path('search')
	}

	
	
	$scope.send_message = function(){
		// socket.emit("sendto", {id: $scope.selected_person.index, msg: {dir: "to", text: $scope.message_text}}, function(response){

		// 	if(response){
		// 		console.log("This person is connected. Time to socketio")
		// 	}
		// 	else{
		// 		console.log("This person is not connected")
		// 	}
		// })
		// MessageService.send_message($scope.selected_person.index, $scope.message_text).then(function(res){
		// 	$scope.selected_person.messages.push({text: $scope.message_text, dir: "from"})
		// 	$scope.message_text = ""
		// })
	}
	$scope.change_person = function(index){
		console.log()
		if($scope.people){
			for (person in $scope.people){
				if($scope.people[person].index == index){
					$scope.selected_person = $scope.people[person]
					$scope.people[person].seen = true
					$scope.selected_person.seen = true
				}
			}	
		}
	}
	$scope.person_selected = function(index){
		if($scope.people){
		if($scope.selected_person.index == index){
			return "chat-selected"
		}
		else{
			return ""
		}
	}
	}
					
}
module.exports = MessagesController