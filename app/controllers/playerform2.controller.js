PlayerForm2Controller.$inject = ["$scope", "$location", "UserService"]
function PlayerForm2Controller($scope, $location, UserService){
	$scope.next_reg = function(){
		UserService.player2($scope.position, $scope.height, $scope.weight, $scope.ACT, $scope.SAT, $scope.WGPA, $scope.UWGPA, $scope.ASVAB, $scope.course, $scope.age).then(function(response){
			if(response.success){
				$location.path('player3') 
			}
		})
	}
}
module.exports = PlayerForm2Controller