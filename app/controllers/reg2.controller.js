Reg2Controller.$inject = ["$scope", "$location", "UserService", "$http", "$stateParams"]
function Reg2Controller($scope, $location, UserService, $http, $stateParams ){

	$scope.editing_phone = function(){
		if($scope.phone != ""){
			$scope.phone_status = "has-success"
			$scope.hide_phone = true
		}
		else{
			$scope.phone_error = "You have not entered a valid phone number"
			$scope.hide_phone = false
			$scope.phone_status = "has-warning"
		}
	}
	$scope.next_reg = function(){
		if($scope.phone_status == "has-success"){
			UserService.updatePhone($scope.phone).then(function(res){
				if(res.success){
					$location.path('reg3/'+$stateParams.type)
				}
				else{
					$scope.phone_error = "You have not entered a valid phone number"
					$scope.hide_phone = false
					$scope.phone_status = "has-warning"
				}
			})

		}  
	}
}
module.exports = Reg2Controller;