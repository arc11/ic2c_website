SideBarController.$inject = ["$scope", "$location", "$state", "$cookies"]
function SideBarController($scope, $location, $state, $cookies){
		console.log("sidebar")
		

		if($cookies.get('username')){
			$scope.username = $cookies.get('username')
		}else{
			$location.path('home')
		}

		var check_state = function(state_name){
			if ($state.current.name === state_name){
				return "active"
			}
			else {
				return ""
			}
		}

		$scope.active1 = check_state("profile")
		$scope.active2 = check_state("feed")
		$scope.active3 = check_state("messages")
		$scope.active4 = check_state("settings")
		$scope.active5 = check_state("search")
		$scope.signout = function(){
			console.log("signing out")
			var cookies = $cookies.getAll();
angular.forEach(cookies, function (v, k) {
    $cookies.remove(k);
});
$state.go("home")
		}
		$scope.set_active1 = function(){
			if($cookies.get('username')){
				var going = "profile/"+$cookies.get('username')
				console.log(going)
				$location.path(going)
			}
			else{
				$state.go('home')
			}
		}
		$scope.set_active2 = function(){
			$state.go('feed')
		}
		$scope.set_active3 = function(){
			$state.go('messages')
		}
		$scope.set_active4 = function(){
			$state.go('settings')
		}
		$scope.set_active5 = function(){
			$state.go('search')
		}



}
module.exports = SideBarController