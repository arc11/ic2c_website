Reg1Controller.$inject = ["$scope", "$location", "UserService", "$cookies", "$http", "$stateParams"]
function Reg1Controller($scope, $location, UserService, $cookies, $http, $stateParams){
	$searchObject = $location.search();
	$scope.user = {}
	$scope.promo = ""
	if($searchObject.affilicaetsessionuuid != null && $searchObject.affilicaethitid != null){
		$scope.affilicaetsessionuuid=$searchObject.affilicaetsessionuuid;
		$scope.affilicaethitid=$searchObject.affilicaethitid;
	}
	$scope.loading_ng_hide = "ng-hide"
	$scope.main_ng_hide = ""
	$scope.user.check = [true, true, true, true, true, true]
	$scope.editing_name = function(){
		if($scope.name != ""){
			$scope.user.check[0] = false
			$scope.name_status = "has-success"
			$scope.hide_name = true
		}
		else{
			$scope.user.check[0] = true
			$scope.name_error = "Please enter your full name."
			$scope.hide_name = false
			$scope.name_status = "has-warning"
		}
	}

	$scope.editing_promo = function(){
		if($scope.promo != ""){
			$scope.user.check[5] = false
			$scope.promo_status = "has-success"
			$scope.hide_promo = true
		}
		else{
			$scope.user.check[5] = true
			$scope.name_error = "Please enter your full name."
			$scope.hide_promo = false
			$scope.promo_status = "has-warning"
		}
	}
	$scope.editing_username = function(){
		if($scope.username != ""){
			$scope.user.check[1] = false
			$scope.username_status = "has-success"
			$scope.hide_username = true
		}
		else{
			$scope.user.check[1] = true
			$scope.username_error = "Please enter a username"
			$scope.hide_username = false
			$scope.username_status = "has-warning"
		}
	}

	$scope.subtypes = ["Monthly Subscription", "Yearly Subscription"]

	$scope.editing_email = function(){
		if($scope.email != "" && $scope.email.indexOf('@') >1){
			$scope.user.check[2] = false
			$scope.email_status = "has-success"
			$scope.hide_email = true
		}
		else{
			$scope.user.check[2] = true
			$scope.email_error = "Please enter an accepted email address"
			$scope.hide_email = false
			$scope.email_status = "has-warning"
		}
	}
	$scope.editing_password = function(){
		if($scope.password.length >= 8){
			$scope.user.check[3] = false
			$scope.password_status = "has-success"
			$scope.hide_password = true
		}
		else{
			$scope.user.check[3] = true
			$scope.password_error = "Your password must be longer than 8 characters"
			$scope.hide_password = false
			$scope.password_status = "has-warning"
		}
	}
	$scope.editing_confirm = function(){
		if($scope.confirm == $scope.password){
			$scope.user.check[4] = false
			$scope.confirm_status = "has-success"
			$scope.hide_confirm = true
		}
		else{
			$scope.user.check[4] = true
			$scope.confirm_error = "Your passwords do not match"
			$scope.hide_confirm = false
			$scope.confirm_status = "has-warning"
		}
	}

	$scope.editing_confirm = function(){
		if($scope.confirm == $scope.password){
			$scope.user.check[4] = false
			$scope.confirm_status = "has-success"
			$scope.hide_confirm = true
		}
		else{
			$scope.user.check[4] = true
			$scope.confirm_error = "Your passwords do not match"
			$scope.hide_confirm = false
			$scope.confirm_status = "has-warning"
		}
	}



	$scope.onToken = function(token) {
		$scope.next_reg(token)
	};
	 
	$scope.onStripe = function(apiKey) {
		console.log($scope.subscription_select)
		if($scope.confirm_status == "has-success" && $scope.password_status == "has-success" &&  $scope.email_status == "has-success" && $scope.username_status == "has-success" && $scope.name_status == "has-success"){
			var handler = StripeCheckout.configure({
				key: apiKey,
				image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
				locale: 'auto',
				token: $scope.onToken
			});
			 

			if($scope.subscription_select == "Monthly Subscription") {
				$scope.plan = "plan_FmySWhaZaCiaQT" //prod
			 	// $scope.plan = "plan_F1j2TzZgP9N6ez" //test

				handler.open({
					panelLabel : 'Subscribe',
					amount : 1999,
					name : 'IC2C Profile (Monthly)',
					description : '$19.99 Monthly Subscription',
					email : $scope.email,
					zipCode : true,
					allowRememberMe : false
				});
			} else if($scope.subscription_select == "Yearly Subscription") {
				$scope.plan = "plan_FmyVvpHqKkoXHO" // prod

				handler.open({
					panelLabel : 'Subscribe',
					amount : 9999,
					name : 'IC2C Profile Yearly',
					description : '$99.99 Yearly Subscription',
					email : $scope.email,
					zipCode : true,
					allowRememberMe : false
				});
			}

		}
	};


	$scope.change_select = function(){
		console.log($scope.subscription_select)
	};

	$scope.next_reg = function(token){
		$scope.loading_ng_hide = ""
		$scope.main_ng_hide = "ng-hide"
		UserService.charge(token, $scope.email, $scope.plan).then(function(charge_res){
			UserService.createUser($scope.name, $scope.username, $scope.email, $scope.password, '', $scope.affilicaetsessionuuid, $scope.affilicaethitid, $stateParams.type).then(function(res){
				if(res.success){
					$http.defaults.headers.common["Authorization"] = res.token;
					$cookies.put("token",res.token)
					$location.path('reg2/'+$stateParams.type)
				}
				else{
					if(res.path == "username"){
						$scope.user.check[1] = true
						$scope.hide_username = false
						$scope.username_error = res.message
					}
					else if(res.path == "email"){
						$scope.user.check[2] = true
						$scope.hide_email = false
						$scope.email_error = res.message
					}
				}
			})
		})

	}

}

module.exports = Reg1Controller;