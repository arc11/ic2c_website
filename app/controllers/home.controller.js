HomeController.$inject = ["$scope", "$location", "$window", "$http", "UserService", "$cookies"]
function HomeController($scope, $location, $window, $http, UserService, $cookies) {
  const GLOBALS = require("../config.globals");

  $scope.appVersion = GLOBALS.APP_VERSION;
  $scope.logonav = 0;
  $scope.toggle = false;
  $scope.user = {}
  $scope.template = "myPopoverTemplate.html"
  $scope.toggle_signin = function () {
    $scope.toggle = !$scope.toggle
  }
  $scope.forgotPassword = function () {
    if ($scope.user.user_email == null) {
      $scope.login_error_text = "Please enter an email and click 'forgot password'"
    }
    else {
      UserService.forgotPasswordEmail($scope.user.user_email).then(function (response) {
        if (response.success) {
          $scope.login_error_text = "An email has been sent to you."
        }
      })
    }
  }
  $scope.signin = function () {
    if ($scope.user.user_email != null && $scope.user.password != null) {
      UserService.login($scope.user.user_email, $scope.user.password).then(function (response) {
        if (response.success == true) {
          $http.defaults.headers.common["Authorization"] = response.token;
          $cookies.put("token", response.token)
          $cookies.put('id', response.user.id)
          $cookies.put('username', response.user.username)
          $cookies.put('name', response.user.name)
          $cookies.put('email', response.user.email)
          $scope.gomain(response.user.username)
        } else {
          $scope.login_error_text = response.message
        }
      })
    }
  }

  $scope.startRegistration = function () {
    $location.path('reg1/player')
  }
  $scope.coachRegistration = function () {
    $location.path('reg1/coach')
  }
  $scope.gomain = function (username) {
    $location.path('profile/' + username)
  }
  $scope.sendContactEmail = function () {

    UserService.sendContactEmail($scope.user.contact_email, $scope.user.contact_name, $scope.user.contact_message).then(function (response) {
      $scope.user = {}
      $location.path('/')
    })
  }
}
module.exports = HomeController