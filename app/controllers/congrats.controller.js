CongratsController.$inject = ["$scope", "$location","$http", "UserService", "$cookies"]
function CongratsController($scope, $location,  $http, UserService, $cookies){
	$scope.signin = function(){
		if($scope.user.user_email != null && $scope.user.password != null){
			UserService.login($scope.user.user_email, $scope.user.password).then(function(response){
				if(response.success == true){
					$http.defaults.headers.common["Authorization"] = response.token;
					$cookies.put("token",response.token)
					$cookies.put('id', response.user.id)
					$cookies.put('username', response.user.username)
					$cookies.put('name', response.user.name)
					$cookies.put('email', response.user.email)
					$scope.gomain(response.user.username)
				}
				else{
					$scope.login_error_text = response.message
				}
			})
		}
	}
	$scope.gomain = function(username){
		$location.path('profile/'+username)
	}
}

module.exports = CongratsController