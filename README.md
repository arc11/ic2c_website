# IC2C Website

### To run in dev
1. npm run rebuild
2. open new shell
3. npm run start
4. make changes
5. refresh browser
6. view changes

webpack should be watching for changes. browser will not automatically refresh.


### Push to production
1. ssh into the server at the instance ec2-34-230-48-95.compute-1.amazonaws.com using the .pem file
2. `tmux a -t server`
3. quit
4. `git pull`
5. `cd app`
6. `webpack`
7. `cd ..`
8. `npm start`

